class Entity {
  constructor(size, position) {
    this.size = size || null;
    this.position = position || null;
  }

  draw(context) {
    context.fillStyle = 'rgb(0,0,0)';
    context.fillRect(
      this.position.x,
      this.position.y,
      this.size.width,
      this.size.height);
  }
}

class Enemy extends Entity {
  constructor(size, position) {
    super(size, position);
    this.host = document.body;
  }

  fire(bullet) {
    let fireEvent = new CustomEvent('fire', {
      detail: { bullet }
    });

    return this.host.dispatchEvent(fireEvent);
  }
}

class Player extends Enemy {
  constructor(screenSize) {
    const size = {
      width: 16,
      height: 16
    };
    const position = {
      x: screenSize.width / 2 - size.width / 2,
      y: screenSize.height - size.height * 2
    };

    super(size, position);

    this.controls = new PlayerControls();
    this.speed = 10;
    this.bullets = 0;
    this.timer = 0;
  }

  update() {
    if (this.controls.isPressed(this.controls.keys.left)) {
      this.position.x -= this.speed;
    } else if (this.controls.isPressed(this.controls.keys.right)) {
      this.position.x += this.speed;
    } else if (this.controls.isPressed(this.controls.keys.space)) {
      if (this.bullets < 5) {
        let position = {
          x: this.position.x + this.size.width / 2 - 1,
          y: this.position.y - 4
        };
        let velocity = {
          x: 0,
          y: -6
        };
        
        this.fire(new Bullet(position, velocity));
        this.bullets++;
      }
    }

    this.timer++;
    if (this.timer % 12 === 0) {
      this.bullets = 0;
    }
  }
}

class PlayerControls {
  constructor() {
    this.keys = {
      left: 37,
      right: 39,
      space: 32
    };
    this.state = {};

    window.addEventListener('keydown', event => this.state[event.keyCode] = true);
    window.addEventListener('keyup', event => this.state[event.keyCode] = false);
  }

  isPressed(keyCode) {
    return this.state[keyCode] === true;
  }
}

class Bullet extends Entity {
  constructor(position, velocity) {
    const size = {
      width: 3,
      height: 2
    };

    super(size, position);

    this.velocity = velocity;
  }

  update() {
    this.position.x += this.velocity.x;
    this.position.y += this.velocity.y;
  }
}

class Invader extends Enemy {
  constructor(position, bodiesRef) {
    const size = {
      width: 16,
      height: 16
    };

    super(size, position);

    this.partolX = 0;
    this.speedX = 0;
    this.bodiesRef = bodiesRef || [];
  }

  /**
  * @description populates new invaders
  * @param {object} config
  *   @member {number} count - invaders count
  *   @member {number} space - space size between invaders in px
  *   @member {number} rows
  *   @member {number} cols
  */
  static pupulate(config) {
    return new Array(config.count)
      .fill(null)
      .map((item, index) => ({
        x: config.space + (index % config.cols) * config.space,
        y: config.space + (index % config.rows) * config.space,
      }))
      .map(position => new Invader(position, config.gameRef.bodies));
  }

  filterInvadersBelow() {
    return this.bodiesRef
      .filter(body => body instanceof Invader)
      .filter(anotherInvader => 
        anotherInvader.position.y > this.position.y &&
        anotherInvader.position.x - this.position.x < this.size.width
      ).length > 0;
  }

  update() {
    if (this.partolX < 0 || this.partolX > 300) {
      this.speedX = -this.speedX;
    }

    this.position.x += this.speedX;
    this.partolX += this.speedX;

    if (Math.random() < 0.01 && !this.filterInvadersBelow()) {
      let position = {
        x: this.position.x + this.size.width / 2 - 1,
        y: this.position.y + this.height / 2
      };

      let velocity = {
        x: Math.random() - 0.5,
        y: 2
      };

      this.fire(new Bullet(position, velocity));
    }
  }

}

class Game {
  constructor(canvasId) {
    let canvas = document.getElementById(canvasId);
    let screen = canvas.getContext('2d');
    let screenSize = {
      width: canvas.width,
      height: canvas.height
    };
    let host = document.body;
    let invadersConfig = {
      count: 6,
      space: 30,
      cols: 3,
      rows: 2,
      gameRef: this
    };

    this.bodies = [
      new Player(screenSize),
      ...Invader.pupulate(invadersConfig)];

    host.addEventListener('fire', 
      event => this.bodies.push(event.detail.bullet));

    let tick = () => {
      this.update(screenSize);
      this.draw(screen, screenSize);
      requestAnimationFrame(tick);
    }

    tick();
  }

  draw(screen, screenSize) {
    screen.clearRect(0, 0, screenSize.width, screenSize.height);
    this.bodies.forEach(body => body.draw(screen));
  }

  update(screenSize) {
    this.bodies.forEach(body => body.update());
  }

}

new Game('game');
